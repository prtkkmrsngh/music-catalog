-- MySQL dump 10.13  Distrib 5.5.44, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: songs
-- ------------------------------------------------------
-- Server version	5.5.44-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_5f412f9a` (`group_id`),
  KEY `auth_group_permissions_83d7f98b` (`permission_id`),
  CONSTRAINT `group_id_refs_id_f4b32aac` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `permission_id_refs_id_6ba0f519` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_37ef4eb4` (`content_type_id`),
  CONSTRAINT `content_type_id_refs_id_d043b34a` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can add permission',2,'add_permission'),(5,'Can change permission',2,'change_permission'),(6,'Can delete permission',2,'delete_permission'),(7,'Can add group',3,'add_group'),(8,'Can change group',3,'change_group'),(9,'Can delete group',3,'delete_group'),(10,'Can add user',4,'add_user'),(11,'Can change user',4,'change_user'),(12,'Can delete user',4,'delete_user'),(13,'Can add content type',5,'add_contenttype'),(14,'Can change content type',5,'change_contenttype'),(15,'Can delete content type',5,'delete_contenttype'),(16,'Can add session',6,'add_session'),(17,'Can change session',6,'change_session'),(18,'Can delete session',6,'delete_session'),(19,'Can add user login',7,'add_userlogin'),(20,'Can change user login',7,'change_userlogin'),(21,'Can delete user login',7,'delete_userlogin'),(25,'Can add songs list',9,'add_songslist'),(26,'Can change songs list',9,'change_songslist'),(27,'Can delete songs list',9,'delete_songslist'),(28,'Can add library',10,'add_library'),(29,'Can change library',10,'change_library'),(30,'Can delete library',10,'delete_library'),(31,'Can add playlist',11,'add_playlist'),(32,'Can change playlist',11,'change_playlist'),(33,'Can delete playlist',11,'delete_playlist'),(34,'Can add user log',12,'add_userlog'),(35,'Can change user log',12,'change_userlog'),(36,'Can delete user log',12,'delete_userlog');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(75) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$12000$Aju9050asHeP$5kDilLQCIClHQ5RwbY+CPKpGjWj4ifNdroK8upTbWHo=','2015-11-03 13:13:03',1,'prateek','','','dev.prateek@outlook.com',1,1,'2015-11-03 13:10:33');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_6340c63c` (`user_id`),
  KEY `auth_user_groups_5f412f9a` (`group_id`),
  CONSTRAINT `group_id_refs_id_274b862c` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `user_id_refs_id_40c41112` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_6340c63c` (`user_id`),
  KEY `auth_user_user_permissions_83d7f98b` (`permission_id`),
  CONSTRAINT `permission_id_refs_id_35d9ac25` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `user_id_refs_id_4dc23c39` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_6340c63c` (`user_id`),
  KEY `django_admin_log_37ef4eb4` (`content_type_id`),
  CONSTRAINT `content_type_id_refs_id_93d2d1f8` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `user_id_refs_id_c0d12874` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_label` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'log entry','admin','logentry'),(2,'permission','auth','permission'),(3,'group','auth','group'),(4,'user','auth','user'),(5,'content type','contenttypes','contenttype'),(6,'session','sessions','session'),(7,'user login','login','userlogin'),(9,'songs list','songs','songslist'),(10,'library','songs','library'),(11,'playlist','songs','playlist'),(12,'user log','songs','userlog');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_b7b81f0c` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('v4i11yjrqban1j7akbz2xa6m7my2pokr','Y2JmMjRlZWVkZmZkYjg0NzZhMDFkNTE0NTU5NjJjM2U0OTA2ZjI4YTp7fQ==','2015-11-17 13:13:13');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login_userlogin`
--

DROP TABLE IF EXISTS `login_userlogin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login_userlogin` (
  `id` varchar(100) NOT NULL,
  `email` varchar(75) NOT NULL,
  `firstname` varchar(25) NOT NULL,
  `lastname` varchar(25) NOT NULL,
  `passwd` longtext NOT NULL,
  `registered_on` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login_userlogin`
--

LOCK TABLES `login_userlogin` WRITE;
/*!40000 ALTER TABLE `login_userlogin` DISABLE KEYS */;
INSERT INTO `login_userlogin` VALUES ('0e1794e6-ead3-41d0-805d-679db4c8645d','dev.prateek@outlook.com','Developer Prateek','Singh','pbkdf2_sha256$12000$HroNE9Zwqh0Y$beAIBeEN9wBwjsNJOQLiEs5a/SywOWLvt+wor7hSNxc=','2015-11-14 05:38:50'),('37c0f754-b3b4-4d69-9456-e9837ffe5e76','prtkkmrsngh@gmail.com','Prateek Kumar','Singh','pbkdf2_sha256$12000$ehdM0AzTfD84$6mX04v8yy6xhaoQGktIcAKk9GAbL/ShjHxB9jWJzjrM=','2015-11-13 20:44:24');
/*!40000 ALTER TABLE `login_userlogin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `songs_library`
--

DROP TABLE IF EXISTS `songs_library`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `songs_library` (
  `id` varchar(100) NOT NULL,
  `user` varchar(75) NOT NULL,
  `added_on` datetime NOT NULL,
  `songslist_id` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `songs_library_a7677d7a` (`songslist_id`),
  CONSTRAINT `songslist_id_refs_id_248ac480` FOREIGN KEY (`songslist_id`) REFERENCES `songs_songslist` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `songs_library`
--

LOCK TABLES `songs_library` WRITE;
/*!40000 ALTER TABLE `songs_library` DISABLE KEYS */;
INSERT INTO `songs_library` VALUES ('7925f84b-0394-45ca-8e7f-23b9c09b3b80','prtkkmrsngh@gmail.com','2015-11-14 09:05:26','54db9a22-4140-44a2-9af4-ae46399d0abe'),('7ab7222c-b75b-4614-9a1e-25b86e0d644f','prtkkmrsngh@gmail.com','2015-11-14 12:27:37','9dae70aa-3826-4525-a672-3a6206694c03'),('8f132cc5-7825-4a5c-9703-dd68487f3d12','prtkkmrsngh@gmail.com','2015-11-13 22:16:07','3579a018-920c-49e3-ad17-03fb82c0749e'),('c9e5bd9f-dcb6-404a-a6f3-f3eeb4e2d184','prtkkmrsngh@gmail.com','2015-11-13 22:16:07','f8e78306-0c40-40c7-a6df-fa4cfc17e90c');
/*!40000 ALTER TABLE `songs_library` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `songs_playlist`
--

DROP TABLE IF EXISTS `songs_playlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `songs_playlist` (
  `id` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  `user` varchar(75) NOT NULL,
  `share` varchar(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `songslist` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `songs_playlist`
--

LOCK TABLES `songs_playlist` WRITE;
/*!40000 ALTER TABLE `songs_playlist` DISABLE KEYS */;
INSERT INTO `songs_playlist` VALUES ('0fcaa923-3606-417e-be01-1d0c82bbd9fd','Playlist2','prtkkmrsngh@gmail.com','private','2015-11-14 11:41:21','[\'9dae70aa-3826-4525-a672-3a6206694c03\', \'f8e78306-0c40-40c7-a6df-fa4cfc17e90c\']'),('5219d7ae-6443-46ea-96ab-332bff6aefdf','Playlist1','prtkkmrsngh@gmail.com','public','2015-11-14 11:41:07','[\'3579a018-920c-49e3-ad17-03fb82c0749e\', \'54db9a22-4140-44a2-9af4-ae46399d0abe\']');
/*!40000 ALTER TABLE `songs_playlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `songs_songslist`
--

DROP TABLE IF EXISTS `songs_songslist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `songs_songslist` (
  `id` varchar(100) NOT NULL,
  `track` longtext NOT NULL,
  `artist` varchar(150) NOT NULL,
  `lyricist` varchar(50) NOT NULL,
  `music_director` varchar(50) NOT NULL,
  `album` longtext NOT NULL,
  `yor` int(11) NOT NULL,
  `genre` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `songs_songslist`
--

LOCK TABLES `songs_songslist` WRITE;
/*!40000 ALTER TABLE `songs_songslist` DISABLE KEYS */;
INSERT INTO `songs_songslist` VALUES ('3579a018-920c-49e3-ad17-03fb82c0749e','sandese aate hain hame tadpate hain','Roopkumar Rathore, Sonu Nigam','Javed Akhtar','Anu Malik','Border',1998,'Romantic'),('4bc6dcbd-7d42-4970-94eb-58239d9cc7c6','jung to chaand roz hoti hai','A Hariharan','Javed Akhtar','Anu Malik','Border',1998,'Patriotic'),('54db9a22-4140-44a2-9af4-ae46399d0abe','kamariya lach ke re baabu zara bacha ke re','Abhijeet, Anuradha Paudwal, Udit Narayan','','Anu Malik','Mela',2000,'Comedy'),('9dae70aa-3826-4525-a672-3a6206694c03','mere khayaalon ki mallika chaaron taraf teri chhaiya re','Abhijeet','Sameer','Anu Malik','Josh',2000,'Romantic'),('f8e78306-0c40-40c7-a6df-fa4cfc17e90c','raadha kaise na jale','Asha Bhonsle, Udit Narayan','Javed Akhtar','AR Rehman','Lagaan',2001,'Romantic');
/*!40000 ALTER TABLE `songs_songslist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `songs_userlog`
--

DROP TABLE IF EXISTS `songs_userlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `songs_userlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(75) NOT NULL,
  `songslist_id` varchar(100) NOT NULL,
  `played_on` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `songs_userlog_a7677d7a` (`songslist_id`),
  CONSTRAINT `songslist_id_refs_id_19bda60a` FOREIGN KEY (`songslist_id`) REFERENCES `songs_songslist` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `songs_userlog`
--

LOCK TABLES `songs_userlog` WRITE;
/*!40000 ALTER TABLE `songs_userlog` DISABLE KEYS */;
INSERT INTO `songs_userlog` VALUES (10,'prtkkmrsngh@gmail.com','3579a018-920c-49e3-ad17-03fb82c0749e','2015-11-14 08:17:15'),(12,'prtkkmrsngh@gmail.com','54db9a22-4140-44a2-9af4-ae46399d0abe','2015-11-14 08:17:22'),(13,'prtkkmrsngh@gmail.com','f8e78306-0c40-40c7-a6df-fa4cfc17e90c','2015-11-14 08:17:25'),(14,'prtkkmrsngh@gmail.com','54db9a22-4140-44a2-9af4-ae46399d0abe','2015-11-14 08:49:19'),(15,'prtkkmrsngh@gmail.com','3579a018-920c-49e3-ad17-03fb82c0749e','2015-11-14 09:02:44'),(16,'prtkkmrsngh@gmail.com','54db9a22-4140-44a2-9af4-ae46399d0abe','2015-11-14 09:03:07'),(17,'prtkkmrsngh@gmail.com','54db9a22-4140-44a2-9af4-ae46399d0abe','2015-11-14 12:27:03'),(18,'prtkkmrsngh@gmail.com','54db9a22-4140-44a2-9af4-ae46399d0abe','2015-11-14 13:06:24'),(19,'prtkkmrsngh@gmail.com','54db9a22-4140-44a2-9af4-ae46399d0abe','2015-11-14 13:06:26'),(20,'prtkkmrsngh@gmail.com','54db9a22-4140-44a2-9af4-ae46399d0abe','2015-11-14 13:06:28'),(21,'prtkkmrsngh@gmail.com','54db9a22-4140-44a2-9af4-ae46399d0abe','2015-11-14 13:06:31'),(22,'prtkkmrsngh@gmail.com','54db9a22-4140-44a2-9af4-ae46399d0abe','2015-11-14 13:06:37');
/*!40000 ALTER TABLE `songs_userlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `songs_userlogin`
--

DROP TABLE IF EXISTS `songs_userlogin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `songs_userlogin` (
  `id` varchar(100) NOT NULL,
  `email` varchar(75) NOT NULL,
  `firstname` varchar(25) NOT NULL,
  `lastname` varchar(25) NOT NULL,
  `passwd` longtext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `songs_userlogin`
--

LOCK TABLES `songs_userlogin` WRITE;
/*!40000 ALTER TABLE `songs_userlogin` DISABLE KEYS */;
/*!40000 ALTER TABLE `songs_userlogin` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-11-14 21:14:21
