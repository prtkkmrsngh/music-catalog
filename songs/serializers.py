from rest_framework import serializers
from songs.models import SongsList


class SongsListSerializer(serializers.ModelSerializer):
	class Meta:
		model = SongsList
		fields = ('id', 'track', 'artist', 'lyricist', 'music_director', 'album', 'yor', 'genre')