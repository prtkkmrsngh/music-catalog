import uuid
from django.db import models

# Create your models here.
class SongsList(models.Model):
	id = models.CharField(max_length=100, primary_key=True, default=uuid.uuid4)
	track = models.TextField(null=False)
	artist = models.CharField(max_length=150)
	lyricist = models.CharField(max_length=50)
	music_director = models.CharField(max_length=50)
	album = models.TextField(null=False)
	yor = models.IntegerField(blank=False, null=False)
	genre = models.CharField(max_length=25)

class Library(models.Model):
	id = models.CharField(max_length=100, primary_key=True, default=uuid.uuid4)
	user = models.EmailField(null=False)
	added_on = models.DateTimeField(auto_now_add=True)
	songslist = models.ForeignKey(SongsList)

class Playlist(models.Model):
	id = models.CharField(max_length=100, primary_key=True, default=uuid.uuid4)
	name = models.CharField(max_length=50)
	user = models.EmailField(null=False)
	share = models.CharField(max_length=10)
	created_on = models.DateTimeField(auto_now_add=True)
	songslist = models.TextField(null=False)

class UserLog(models.Model):
	user = models.EmailField(null=False)
	songslist = models.ForeignKey(SongsList)
	played_on = models.DateTimeField(auto_now_add=True)