import json
import ast
from django.http import HttpResponse, Http404
from django.shortcuts import render
from django.views.decorators.http import require_POST
from rest_framework.renderers import JSONRenderer
from songs.models import SongsList, Library, Playlist, UserLog
from django.db.models import Count
from serializers import SongsListSerializer

# Create your views here.
@require_POST
def allsongs(request):
	try:
		sl = []
		fil = UserLog.objects.values('songslist__genre').annotate(c=Count('songslist')).order_by('-c')
		if not fil:
			songs = SongsList.objects.all()
		else:
			index = 0
			for f in fil:
				sl.append(fil[index]['songslist__genre'])
				index += 1
			songs = SongsList.objects.filter(genre__in=sl).all()
		serializer = SongsListSerializer(songs, many=True)
		json = JSONRenderer().render(serializer.data)
		return HttpResponse(json)
	except Exception as e:
		print('Error: %s' % e)

@require_POST
def top10(request):
	try:
		sl = []
		fil = UserLog.objects.values('songslist').annotate(c=Count('songslist')).order_by('-c')[:10]
		if not fil:
			songs = SongsList.objects.all()
		else:
			index = 0
			for f in fil:
				sl.append(fil[index]['songslist'])
				index += 1
			songs = SongsList.objects.filter(id__in=sl).all()
		serializer = SongsListSerializer(songs, many=True)
		json = JSONRenderer().render(serializer.data)
		return HttpResponse(json)
	except Exception as e:
		print('Error: %s' % e)

@require_POST
def libsongs(request):
	try:
		user = request.POST.get('user')
		sl = []
		fil = Library.objects.filter(user=user).values('songslist').distinct()
		index = 0
		for f in fil:
			sl.append(fil[index]['songslist'])
			index += 1
		songs = SongsList.objects.filter(id__in=sl).all()
		serializer = SongsListSerializer(songs, many=True)
		json = JSONRenderer().render(serializer.data)
		return HttpResponse(json)
	except Exception as e:
		print('Error: %s' % e)

@require_POST
def addToLibrary(request):
	try:
		songsID = ast.literal_eval(request.POST.get("songs"))
		user = request.POST.get("user")
		for s in songsID:
			sl = SongsList.objects.get(id=s)
			Library(user=user, songslist=sl).save()
		return HttpResponse("Added Successfully")
	except Exception as e:
		print('Error: %s' % e)

@require_POST
def createPlaylist(request):
	try:
		name = request.POST.get("name")
		songsID = ast.literal_eval(request.POST.get("songs"))
		user = request.POST.get("user")
		Playlist(name=name, user=user, share='private', songslist=songsID).save()
		return HttpResponse("Created Successfully")
	except Exception as e:
		print('Error: %s' % e)

@require_POST
def viewPlaylist(request):
	try:
		user = request.POST.get("user")
		fil = Playlist.objects.values('id', 'name', 'created_on', 'share').filter(user=user).distinct()
		json = JSONRenderer().render(fil)
		return HttpResponse(json)
	except Exception as e:
		print('Error: %s' % e)

@require_POST
def changeShare(request):
	try:
		playlist_id = request.POST.get("playlist_id")
		share = request.POST.get("share")
		Playlist.objects.filter(id=playlist_id).update(share=share)
		return HttpResponse("Changed")
	except Exception as e:
		print('Error: %s' % e)

@require_POST
def playlistSongs(request):
	try:
		pID = request.POST.get('pID')
		fil = Playlist.objects.filter(id=pID).values('songslist')
		sl = ast.literal_eval(fil[0]['songslist'])
		songs = SongsList.objects.filter(id__in=sl).all()
		serializer = SongsListSerializer(songs, many=True)
		json = JSONRenderer().render(serializer.data)
		return HttpResponse(json)
	except Exception as e:
		print('Error: %s' % e)

@require_POST
def viewPubPlaylist(request):
	try:
		fil = Playlist.objects.values('id', 'name', 'created_on', 'share').filter(share='public').distinct()
		json = JSONRenderer().render(fil)
		return HttpResponse(json)
	except Exception as e:
		print('Error: %s' % e)

@require_POST
def userLog(request):
	try:
		songID = request.POST.get("songID")
		user = request.POST.get("user")
		sl = SongsList.objects.get(id=songID)
		UserLog(user=user, songslist=sl).save()
		return HttpResponse("Saved")
	except Exception as e:
		print('Error: %s' % e)