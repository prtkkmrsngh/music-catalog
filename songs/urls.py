from django.conf.urls import patterns, url
from songs import views

urlpatterns = patterns('',
	url(r'^allsongs/', views.allsongs, name='allsongs'),
	url(r'^top10/', views.top10, name='top10'),
	url(r'^libsongs/', views.libsongs, name='libsongs'),
	url(r'^addToLibrary/', views.addToLibrary, name='addToLibrary'),
	url(r'^createPlaylist/', views.createPlaylist, name='createPlaylist'),
	url(r'^changeShare/', views.changeShare, name='changeShare'),
	url(r'^viewPlaylist/', views.viewPlaylist, name='viewPlaylist'),
	url(r'^playlistSongs/', views.playlistSongs, name='playlistSongs'),
	url(r'^viewPubPlaylist/', views.viewPubPlaylist, name='viewPubPlaylist'),
	url(r'^userLog/', views.userLog, name='userLog'),
)