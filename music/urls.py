from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'music.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^$', include('portal.urls')),
    url(r'^home/', include('portal.urls')),
    url(r'^about/', include('portal.urls')),
    url(r'^top10/', include('portal.urls')),
    url(r'^library/', include('portal.urls')),
    url(r'^playlist/', include('portal.urls')),
    url(r'^playlist/(.+)/', include('portal.urls')),
    url(r'^pub_playlist/', include('portal.urls')),
    url(r'^partials/', include('partials.urls')),
    url(r'^login/', include('login.urls')),
    url(r'^songs/', include('songs.urls')),
    url(r'^admin/', include(admin.site.urls)),
)