angular.module('musicStore').controller('loginController', ['$scope', '$rootScope', '$location', '$http', 'AuthenticationService', '$timeout', '$httpParamSerializerJQLike', function($scope, $rootScope, $location, $http, AuthenticationService, $timeout, $httpParamSerializerJQLike) {
    if($rootScope.globals.currentUser){
        $location.path("/home");
    }else{
        $scope.success = false;
        $scope.error = false;
        $scope.master = {};
        $scope.reset = function() {
          $scope.user = angular.copy($scope.master);
        };
        $scope.register = function () {
            $scope.dataLoading = true;
            $http({
                method: 'POST',
                url: '/login/signup/',
                data: $httpParamSerializerJQLike($scope.user)
            })
            .success(function (data){
                $scope.reset();
                $scope.storeSignup.$setPristine();
                $scope.storeSignup.$setValidity();
                $scope.dataLoading = false;
                if(data == "success"){
                    $scope.success = true;
                }else{
                    $scope.error = true;
                }
                $timeout(function() {
                    $scope.success = false;
                    $scope.error = false;
                }, 3000);
            }).error(function (data){
                $scope.data = data || "Request Failed";
            });
        };

        $scope.login = function () {
            $scope.dataLoading = true;
            AuthenticationService.Login($scope.user.email, $scope.user.passwd, function(response) {
                if(response.success) {
                    AuthenticationService.SetCredentials($scope.user.email, $scope.user.passwd, response.name);
                    $scope.reset();
                    $scope.storeLogin.$setPristine();
                    $scope.storeLogin.$setValidity();
                    $scope.dataLoading = false;
                    $rootScope.profilename = $rootScope.globals.currentUser.name;
                    $rootScope.loginCheck = true;
                    $location.path("/home");
                }else{
                    $scope.error = response.message;
                    $scope.reset();
                    $scope.storeLogin.$setPristine();
                    $scope.storeLogin.$setValidity();
                    $scope.dataLoading = false;
                    $timeout(function() {
                        $scope.error = false;
                    }, 3000);
                }
            });
        };
    };
}]);

angular.module('musicStore').controller('navController', ['$scope', '$rootScope', '$location', '$http', 'AuthenticationService', function($scope, $rootScope, $location, $http, AuthenticationService) {
    if($rootScope.globals.currentUser){
        $rootScope.loginCheck = true;
        $rootScope.profilename = $rootScope.globals.currentUser.name;
    }
    $scope.logout = function () {
        // reset login status
        AuthenticationService.ClearCredentials();
        $rootScope.loginCheck = false;
        $scope.error = "";
        $location.path('/');
    };
}]);

angular.module('musicStore').controller('homeController', ['$scope', '$rootScope', '$location', '$http', '$routeParams', '$httpParamSerializerJQLike', function($scope, $rootScope, $location, $http, $routeParams, $httpParamSerializerJQLike) {
    $scope.titleTip = function() {
        $scope.$broadcast('event:tooltip');
    };
    $scope.allSongs = function() {
        $http({ 
            method: 'POST',
            url: '/songs/allsongs/'
        })
        .success(function (data){
            $scope.songs = data;
        }).error(function (data){
            $scope.data = data || "Request Failed";
        });
    };
    $scope.top10 = function() {
        $http({ 
            method: 'POST',
            url: '/songs/top10/'
        })
        .success(function (data){
            $scope.songs = data;
        }).error(function (data){
            $scope.data = data || "Request Failed";
        });
    };
    $scope.libSongs = function() {
        $http({ 
            method: 'POST',
            url: '/songs/libsongs/',
            data: $httpParamSerializerJQLike({'user' : $rootScope.globals.currentUser.username})
        })
        .success(function (data){
            $scope.songs = data;
        }).error(function (data){
            $scope.data = data || "Request Failed";
        });
    };
    $scope.selection=[];
      // toggle selection for a given employee by name
    $scope.toggleSelection = function toggleSelection(songID) {
        var idx = $scope.selection.indexOf(songID);
        // is currently selected
        if (idx > -1){
            $scope.selection.splice(idx, 1);
        }
        // is newly selected
        else{
            $scope.selection.push(songID);
        }
    };
    $scope.addToLibrary = function() {
        var jsonData = JSON.stringify($scope.selection);
        $http({
            method: 'POST',
            url: '/songs/addToLibrary/',
            data: $httpParamSerializerJQLike({'user' : $rootScope.globals.currentUser.username, 'songs' : jsonData})
        })
        .success(function (data){
            $location.path("/library");
        }).error(function (data){
            $scope.data = data || "Request Failed";
        });
    };
    $scope.createPlaylist = function() {
        var jsonData = JSON.stringify($scope.selection);
        var name = prompt("Please enter playlist name?", "Playlist1");
        if(name != null){
            $http({
                method: 'POST',
                url: '/songs/createPlaylist/',
                data: $httpParamSerializerJQLike({'name' : name, 'user' : $rootScope.globals.currentUser.username, 'songs' : jsonData})
            })
            .success(function (data){
                $location.path("/playlist");
            }).error(function (data){
                $scope.data = data || "Request Failed";
            });
        }
    };
    $scope.userLog = function userLog(songID) {
        $http({
            method: 'POST',
            url: '/songs/userLog/',
            data: $httpParamSerializerJQLike({'user' : $rootScope.globals.currentUser.username, 'songID' : songID})
        })
        .success(function (data){
            
        }).error(function (data){
            $scope.data = data || "Request Failed";
        });
    };
    $scope.playlist = function() {
        $http({ 
            method: 'POST',
            url: '/songs/viewPlaylist/',
            data: $httpParamSerializerJQLike({'user' : $rootScope.globals.currentUser.username})
        })
        .success(function (data){
            $scope.play = data;
        }).error(function (data){
            $scope.data = data || "Request Failed";
        });
    };
    $scope.sharePlay = function sharePlay(share, id) {
        $http({ 
            method: 'POST',
            url: '/songs/changeShare/',
            data: $httpParamSerializerJQLike({'share' : share, 'playlist_id' : id})
        })
        .success(function (data){

        }).error(function (data){
            $scope.data = data || "Request Failed";
        });
    };
    $scope.playlistSongs = function() {
        $http({ 
            method: 'POST',
            url: '/songs/playlistSongs/',
            data: $httpParamSerializerJQLike({'pID' : $routeParams.pID})
        })
        .success(function (data){
            $scope.songs = data;
        }).error(function (data){
            $scope.data = data || "Request Failed";
        });
    };
    $scope.pub_playlist = function() {
        $http({ 
            method: 'POST',
            url: '/songs/viewPubPlaylist/'
        })
        .success(function (data){
            $scope.play = data;
        }).error(function (data){
            $scope.data = data || "Request Failed";
        });
    };
}]);