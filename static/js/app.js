angular.module('musicStore', [
    'ngRoute',
    'ngCookies'
])
.config(function($routeProvider,$locationProvider,$httpProvider) {
    $locationProvider.html5Mode(true).hashPrefix('!');
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=utf-8';
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
    $routeProvider
        // route for the login page
        .when('/', {
            templateUrl : 'partials/login',
            controller  : 'loginController'
        })
        // route for the home page
        .when('/home', {
            templateUrl : 'partials/home',
            controller  : 'homeController'
        })
        // route for the about page
        .when('/about', {
            templateUrl : 'partials/about',
            controller  : 'homeController'
        })
        // route for the home page
        .when('/top10', {
            templateUrl : 'partials/top10',
            controller  : 'homeController'
        })
        // route for the library page
        .when('/library', {
            templateUrl : 'partials/library',
            controller  : 'homeController'
        })
        // route for the playlist page
        .when('/playlist', {
            templateUrl : 'partials/playlist',
            controller  : 'homeController'
        })
        // route for the playlist page
        .when('/playlist/:pID/:pName', {
            templateUrl : 'partials/playlistSongs',
            controller  : 'homeController'
        })
        // route for the pub_playlist
        .when('/pub_playlist', {
            templateUrl : 'partials/pub_playlist',
            controller  : 'homeController'
        })
})
.run(['$rootScope', '$location', '$cookieStore', '$http',
    function ($rootScope, $location, $cookieStore, $http) {
        // keep user logged in after page refresh
        $rootScope.globals = $cookieStore.get('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
        }
        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            // redirect to login page if not logged in
            if ($location.path() !== '/' && !$rootScope.globals.currentUser) {
                $rootScope.loginCheck = false;
                $location.path("/");
            };
        });
    }
]);