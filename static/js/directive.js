angular.module('musicStore').directive('tooltip', function() {
    return function(scope, elem, attrs) {
        scope.$on('event:tooltip', function() {
            elem.tooltip();
        });
    };
});