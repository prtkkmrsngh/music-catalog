from django.conf.urls import patterns, url
from partials import views

urlpatterns = patterns('',
    url(r'^login/', views.login, name='login'),
    url(r'^home/', views.home, name='home'),
    url(r'^about/', views.about, name='about'),
    url(r'^top10/', views.top10, name='top10'),
    url(r'^library/', views.library, name='library'),
    url(r'^playlist/', views.playlist, name='playlist'),
    url(r'^playlistSongs/', views.playlistSongs, name='playlistSongs'),
    url(r'^pub_playlist/', views.pub_playlist, name='pub_playlist'),
)