from django.http import HttpResponse, Http404
from django.shortcuts import render

def login(request):
    return render(request, 'partials/login.html')

def home(request):
    return render(request, 'partials/home.html')

def about(request):
    return render(request, 'partials/about.html')

def top10(request):
    return render(request, 'partials/top10.html')

def library(request):
    return render(request, 'partials/library.html')

def playlist(request):
    return render(request, 'partials/playlist.html')

def playlistSongs(request):
    return render(request, 'partials/playlistSongs.html')

def pub_playlist(request):
    return render(request, 'partials/pub_playlist.html')