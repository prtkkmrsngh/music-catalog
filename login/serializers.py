from rest_framework import serializers
from login.models import UserLogin


class UserLoginSerializer(serializers.ModelSerializer):
	class Meta:
		model = UserLogin
		fields = ('email', 'firstname', 'lastname', 'passwd')