from django.conf.urls import patterns, url
from login import views

urlpatterns = patterns('',
	url(r'^signup/', views.signup, name='signup'),
	url(r'^signin/', views.signin, name='signin'),
)