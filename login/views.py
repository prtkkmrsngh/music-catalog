import json
from django.http import HttpResponse, Http404
from django.contrib.auth.hashers import make_password, check_password
from django.shortcuts import render
from django.views.decorators.http import require_POST
from models import UserLogin
from serializers import UserLoginSerializer
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser

@require_POST
def signup(request):
	try:
		serializer = UserLoginSerializer(data=request.POST)
		if serializer.is_valid():
			serializer.object.email = serializer.object.email.lower()
			serializer.object.passwd = make_password(serializer.object.passwd)
			serializer.save()
			return HttpResponse("success")
		return HttpResponse("exists")
	except Exception as e:
		print('Error: %s' % e)


@require_POST
def signin(request):
	try:
		response = {}
		user = UserLogin.objects.get(email=request.POST.get("email").lower())
		pass_key = check_password(request.POST.get("passwd"), user.passwd)
		if bool(pass_key) == True:
			response = {"success": True, "name": user.firstname}
			jsond = JSONRenderer().render(response)
			return HttpResponse(jsond)
		else:
			response = {"success": False}
			jsond = JSONRenderer().render(response)
			return HttpResponse(jsond)
	except Exception as e:
		print('Error: %s' % e)