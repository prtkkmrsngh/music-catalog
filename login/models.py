import uuid
from django.db import models

# Create your models here.
class UserLogin(models.Model):
	id = models.CharField(max_length=100, primary_key=True, default=uuid.uuid4)
	email = models.EmailField(unique=True)
	firstname = models.CharField(max_length=25)
	lastname = models.CharField(max_length=25)
	passwd = models.TextField(null=False)
	registered_on = models.DateTimeField(auto_now_add=True)